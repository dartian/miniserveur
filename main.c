#include <stdio.h>
#include <string.h>   //strlen
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>   //close
#include <arpa/inet.h>    //close
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros
#include <pthread.h>  

#define TRUE   1
#define FALSE  0
#define PORT 8888
#define MAX_CLIENT 255

typedef struct Client Client;
struct Client{
	int nb;
	int etat;//bool 0 actife, 1 innactife
};


int client_socket[MAX_CLIENT], max_clients = MAX_CLIENT;
Client client[MAX_CLIENT];


void *receive_message(void *arg){

	char str[1025];

	while(TRUE){
		
		scanf("%s", str);
		
		if(strcmp(str, "get_list") == 0){
			int j;			
			for (j = 0; j < max_clients; j++) 
        		{
				if(client_socket[j]!=0){
					char message[1052];
					sprintf(message, "Client %d\n", j);
					printf("%s", message);
				}							
			}
			
		}else if(strstr(str, "get_state")){
			int i = 0;
			int pos = -1,  start, end;
			
			for(i; i < strlen(str); i++){
				if(str[i] == '['){
					start = i+1;
				}else if(str[i] == ']'){
					end = i-1;
				}
			}

			if(start != end){
				char poschar[3];
				while(start <= end){
					strcat(poschar, str[start]);
					start++;
				}
				pos = poschar - '0';
			}else{
				//printf("%c", str[start]);
				pos = str[start] - '0';	
				//printf("%d", pos);			
			}


			if(pos != -1){
			printf("%d\n", client_socket[pos]);
				if(client_socket[pos]!=0){					
					char message[1052];
					if(client[pos].etat == 0)
						sprintf(message, "Client %d, Etat: actif\n", pos);
					else
						sprintf(message, "Client %d, Etat: innactif\n", pos);
					printf("%s", message);
				}
			}
		
		}

		memset( str, 0, sizeof(str) );
	}
}



int main(int argc , char *argv[])
{
    int opt = TRUE;
    int master_socket , addrlen , new_socket  , activity, i , valread , sd;
    int max_sd;
    struct sockaddr_in address;
      
    char buffer[1025];
      
    fd_set readfds;
      
    char *message = "ECHO Daemon v1.0 \r\n";
  
    pthread_t threadArray[2];

    if(pthread_create(&threadArray[0], NULL, receive_message, NULL)){
		perror("creation du thread");
		return (EXIT_FAILURE);
	}

    for (i = 0; i < max_clients; i++) 
    {
        client_socket[i] = 0;
    }
      
    if( (master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0) 
    {
        perror("socket fail");
        exit(EXIT_FAILURE);
    }
  
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 )
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
  
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    printf("Ecoute sur le port %d \n", PORT);
     
    if (listen(master_socket, 3) < 0)
    {
        perror("ecoute");
        exit(EXIT_FAILURE);
    }
      
    addrlen = sizeof(address);
    puts("Attend des connections ...");
     
    while(TRUE) 
    {
        FD_ZERO(&readfds);
  
        FD_SET(master_socket, &readfds);
        max_sd = master_socket;
         
        for ( i = 0 ; i < max_clients ; i++) 
        {
            sd = client_socket[i];
             
            if(sd > 0)
                FD_SET( sd , &readfds);
             
            if(sd > max_sd)
                max_sd = sd;
        }
  
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);
    
        if ((activity < 0) && (errno!=EINTR)) 
        {
            printf("select error");
        }
          
        if (FD_ISSET(master_socket, &readfds)) 
        {
            if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0)
            {
                perror("accept");
                exit(EXIT_FAILURE);
            }
          
            printf("Nouvelle connection , socket fd est %d , ip est : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
          
            if( send(new_socket, message, strlen(message), 0) != strlen(message) ) 
            {
                perror("envoyer");
            }
              
            puts("Bonjour message envoyer correctement");
              
            for (i = 0; i < max_clients; i++) 
            {
                if( client_socket[i] == 0 )
                {
                    client_socket[i] = new_socket;
                    printf("Ajouter à une liste de socket %d\n" , i);
		    Client new_client;
		    new_client.nb = i;
		    new_client.etat = 0;
                    client[i] = new_client;
		    char message[1052];
		    sprintf(message, "Client numero %d\n", i);
		    send(new_socket, message, strlen(message), 0);
                    break;
                }
            }
        }
          

        for (i = 0; i < max_clients; i++) 
        {
            sd = client_socket[i];
              
            if (FD_ISSET( sd , &readfds)) 
            {
                if ((valread = read( sd , buffer, 1024)) == 0)
                {

                    getpeername(sd , (struct sockaddr*)&address , (socklen_t*)&addrlen);
                    printf("Host deconnecter , ip %s , port %d \n" , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
                      
                    close( sd );
                    client_socket[i] = 0;
                }
                  
                else
                {
			int lgt = strlen(buffer);
			char message[lgt-2];
			memcpy( message, buffer, lgt-2 );
			message[lgt-2] = '\0';
			
		    if(strcmp(message, "send_state")==0){
			Client cl = client[i];
			
			if(client[i].etat == 1){
				client[i].etat = 0;
				sprintf(message, "Etat: actif\n");
			}
			else{
				client[i].etat = 1;
				sprintf(message, "Etat: inactif\n");
			}
		        message[strlen(message)] = '\0';
			send(sd, message, strlen(message), 0);
			
			memset( buffer, 0, sizeof(buffer) );	
		    }
		    else if(strcmp(message, "deco")==0){
		    	    close( sd );
	                    client_socket[i] = 0;
			    message[valread]= '\0';
			    buffer[0] = '\0';
			    memset( buffer, 0, sizeof(buffer) );
		    }
		    else if(strcmp(message, "get_state") == 0){
			    if(client[i].etat == 0){
				
				sprintf(message, "Etat: actife\n");
			}
			else{
				
				sprintf(message, "Etat: innactife\n");
			}
			message[strlen(message)] = '\0';
			send(sd, message, strlen(message), 0);
		
			memset( buffer, 0, sizeof(buffer) );	
		    }
		    else{
			   printf("Message: %s, envoyer par la socket %d\n ", message, sd);
		            buffer[valread-2] = '\0';
			    message[valread-2]= '\0';
		            buffer[0] = '\0';
			    memset( buffer, 0, sizeof(buffer) );
		   }
                }
            }
        }
    }
      
    return 0;
} 
